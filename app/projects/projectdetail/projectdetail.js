var projectDetail = angular.module('jkApp.projectDetail', [
  'ui.router',
  'ngAnimate',
  'jkApp',
  'jkApp.projects',
  // 'ngSanitize'
]);

jkApp.config(function($stateProvider){
  $stateProvider
  .state('project.detail', {
    url: "/:picture",
    views: {
      'galleryImage': {
        templateUrl: 'projects/projectdetail/galleryimage.tmpl.html',
      }
    }
  })

});

projectDetail.controller('projectDetailCtrl', ['$scope', '$state','$stateParams', '$filter', '$rootScope', '$timeout', '$window', '$sce'  , projectDetailCtrl]);

function projectDetailCtrl($scope, $state, $stateParams, $filter, $rootScope, $timeout, $window, $sce){
  var vm = this;

  vm.toTrustedHTML = function( txt ){
    return $sce.trustAsHtml( txt );
  }

  vm.loadMoreLimit = 9;
  vm.prevBtn = false;
  vm.nextBtn = false;
  vm.galleryShow = { start: 0  , end: vm.loadMoreLimit };
  vm.galleryImgMainState = null;
  // vm.galleryImgMainLoading = false;
  vm.smallGalleryHide = false;

  var galleryImgAniTime = 300;

  vm.galleryShowChange = function(direction){
    if(direction == 'next'){
      vm.galleryShow.start += vm.loadMoreLimit;
      vm.galleryShow.end += vm.loadMoreLimit;
      vm.galleryShow.end >= vm.projectData.gallery.length ?  vm.nextBtn = false :  vm.nextBtn = true;
      vm.prevBtn = true;
    }else{
      vm.galleryShow.start -= vm.loadMoreLimit;
      vm.galleryShow.end -= vm.loadMoreLimit;
      if(vm.galleryShow.start == 0)
      vm.prevBtn = false;
      if(vm.galleryShow.end <= vm.projectData.gallery.length)
      vm.nextBtn = true;
    }
  }

  vm.galleryImgNextPrev = function(direction){
    vm.galleryImgMainState = 'out loading';
    $timeout(function(){
      $state.go( 'project.detail' , {picture: vm.galleryPictureCurrent + direction } );
    },galleryImgAniTime);
  }

  vm.galleryImgChangeTo = function(i){
    if($state.params.picture == i && $window.innerWidth > 768) return;
    vm.galleryImgMainState = 'out loading';
    $timeout(function(){
      $state.go( 'project.detail' , { picture: i + vm.galleryShow.start } );
      $window.innerWidth <= 768 ?  vm.smallGalleryHide = true  : vm.smallGalleryHide = false;
    },galleryImgAniTime);
  }

  vm.projectData;
  var projects = $scope.projects;

  vm.createGallery = function(){
    vm.projectData.gallery = [];
    for (var i = 1 ; i < vm.projectData.galleryLength + 1; i++ ) {
      var imgName = i < 10 ? '0' + i : i ;
      vm.projectData.gallery.push( [imgName + vm.projectData.galleryFormat , imgName + 's' + vm.projectData.galleryFormat] );
    }
  }

  $scope.$on('$viewContentLoaded', function(event) {
    $timeout(function () {
      projects = $scope.projects;

      vm.projectData =  $filter('filter')(projects, $stateParams.project)[0];
      if($state.current.name == 'project' && $state.params.picture == undefined){
        $state.go('project.detail', { 'picture': '0' } );
        vm.createGallery();
        // $window.innerWidth <= 768 ?  vm.smallGalleryHide = true  : vm.smallGalleryHide = false;
      }

      if(!vm.projectData.gallery){
        vm.createGallery();
      }

      vm.galleryPictureCurrent = Number($state.params.picture);

      if(vm.galleryPictureCurrent >= vm.projectData.gallery.length){
        vm.galleryPictureCurrent = 0;
        $state.go( $state.current.name , {project : $state.params.project, picture: vm.galleryPictureCurrent } );
      }else if(vm.galleryPictureCurrent <= -1){
        vm.galleryPictureCurrent = vm.projectData.gallery.length -1;
        $state.go( $state.current.name , {project : $state.params.project, picture: vm.galleryPictureCurrent } );
      }

      var interval = Math.floor(vm.galleryPictureCurrent / vm.loadMoreLimit) * vm.loadMoreLimit
      vm.galleryShow = { start: interval  , end:  interval + vm.loadMoreLimit };

      vm.galleryShow.start < vm.loadMoreLimit ?  vm.prevBtn = false : vm.prevBtn = true;
      vm.galleryShow.end >= vm.projectData.gallery.length ?  vm.nextBtn = false :  vm.nextBtn = true;

    },0);
  });

};


//***********************************************************************
//-> DIRECTIVES
//************************************************************************

projectDetail.directive('galImgLoader', function($timeout){
  return{
    link: function(scope, element, attrs){
      element.unbind("load");

      element.bind("load",function(){
        scope.vm.galleryImgMainState = 'loading' ;
        $timeout(function(){
          scope.vm.galleryImgMainState = 'in' ;
        },150);
      });
    }
  }
});

projectDetail.directive('swipeGalImg', function($timeout , $document){
  return{
    link: function(scope, element, attrs){

      var $galleryImgSlider = element[0];
      var hammer    = new Hammer.Manager($galleryImgSlider);
      var swipe     = new Hammer.Swipe();
      hammer.add(swipe);
      hammer.on('swipeleft', function(){
        scope.vm.galleryImgNextPrev(1);
      });
      hammer.on('swiperight', function(){
        scope.vm.galleryImgNextPrev(-1);
      });

      scope.$on('$destroy', function() {
        hammer.destroy();
      });

    }
  }
});

projectDetail.directive('galSwipeNextProjects', function($timeout , $document){
  return{
    link: function(scope, element, attrs){

      var $galleryImgSlider = element[0];
      var hammer    = new Hammer.Manager($galleryImgSlider);
      var swipe     = new Hammer.Swipe();
      hammer.add(swipe);
      hammer.on('swiperight', function(){
        if(scope.vm.prevBtn == false) return;
        scope.vm.galleryShowChange('prev');
        scope.$apply();
      });
      hammer.on('swipeleft', function(){
        if(scope.vm.nextBtn == false) return;
        scope.vm.galleryShowChange('next');
        scope.$apply();
      });

      scope.$on('$destroy', function() {
        hammer.destroy();
      });

    }
  }
});

projectDetail.directive('keysGalImg', function($timeout , $document){
  return{
    link: function(scope, element, attrs){

      $document.unbind('keyup');

      $document.bind( 'keyup', function( event ){
        if (event.which === 39){
          scope.vm.galleryImgNextPrev(1);
        } else if (event.which === 37) {
          scope.vm.galleryImgNextPrev(-1);
        }
      });

    }
  }
});
