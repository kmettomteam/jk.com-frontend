var jkApp = angular.module('jkApp', [
  'ui.router',
  'ngAnimate',
  'jkApp.projects',
  'jkApp.projectDetail',
  'jkApp.contact',
  'jkApp.templates',
  // 'ngSanitize',
  // 'templates'
]);


jkApp.config(function($stateProvider){
  $stateProvider
  .state('hello', {
    url: "/",
    views: {
      'main': {
        templateUrl: 'hello.tmpl.html',
        controller: 'helloCtrl as vm'
      }
    }
  })
  .state('analog', {
    url: "/analog",
    views: {
      'main': {
        templateUrl: 'projects/projects.tmpl.html',
        controller: 'projectsCtrl as vm'
      },
    }
  })
  .state('digital', {
    url: "/digital",
    views: {
      'main': {
        templateUrl: 'projects/projects.tmpl.html',
        controller: 'projectsCtrl as vm'
      },
    }
  })
  .state('project', {
    url: "/project/:category/:project",
    views: {
      'main': {
        templateUrl: 'projects/projectdetail/projectdetail.tmpl.html',
        controller: 'projectDetailCtrl as vm'
      }
    }
  })
  .state('about', {
    url: "/about",
    views: {
      'main': {
        templateUrl: 'about/about.tmpl.html',
      },
    }
  })
  .state('contact', {
    url: "/contact",
    views: {
      'main': {
        templateUrl: 'contact/contact.tmpl.html',
        controller: 'contactCtrl as vm'

      },
    }
  })
});

jkApp.controller('mainCtrl', ['$scope', '$state', '$rootScope', '$window', '$document', '$timeout', 'projectsData', '$window', '$templateCache', '$sce' ,  mainCtrl]);
jkApp.controller('helloCtrl', ['$scope', '$state',  '$rootScope', helloCtrl]);
jkApp.service('projectsData',['$http' , projectsData]);



//**********************************
// Projects Data Service
//***********************************

function projectsData($http) {
  var model = this;
  var URLS = {
    FETCH: 'data/gallery-projects2.json'
  };
  var projects;
  model.getProjectsData = function() {
    return $http.get(URLS.FETCH);
  };
};

//**********************************
// MAIN controller
//***********************************

function mainCtrl($scope, $state, $rootScope,$window, $document,  $timeout, projectsData , $window, $templateCache, $sce) {
  var vm = this;

  if(window.navigator.userAgent.indexOf("Safari") > -1 && window.navigator.userAgent.indexOf("Chrome") == -1 ){
    $document[0].body.classList.add('safari');
  }

  var menuHtml = $templateCache.get('menu/menu.tmpl.html');
  vm.menu = $sce.trustAsHtml(menuHtml);

  if($window.location.hash == "") $state.go('hello');

  projectsData.getProjectsData().then(function(result){
    $scope.projects = result.data;
  });

  vm.stateChangeClasses  = { project:'left-right', hello: 'top-bottom' , analog: 'top-bottom', digital: 'bottom-top', about: 'left-right', contact: 'right-left' };
  vm.changeClass = null;
  vm.overlayAniTime = 500;

  vm.menuHide = false;


  vm.changeClassUpdate = function(toState){ // changeState
    var fromState = $state.current.name;
    if(vm.stateChangeClasses[toState]){
      if(fromState == 'project.detail' && toState == 'project.detail') return;
      if(toState == 'project.detail' && fromState == 'analog' ){
        vm.changeClass = vm.stateChangeClasses['analog'];
      }else if(toState == 'project.detail' && fromState == 'digital'){
        vm.changeClass = vm.stateChangeClasses['digital'];
      }else{
        vm.changeClass = vm.stateChangeClasses[toState];
      }
    }
  };

  vm.menuStateChange = function(stateTo, project, category){
    if($state.current.name == stateTo) return;
    vm.changeClassUpdate(stateTo);
    vm.stateChangeActive = true;

    $timeout(function(){
      if(project == null){
        $state.go(stateTo, {});
      }else {
        $state.go(stateTo, {'category': category , 'project' : project});
      }
    },vm.overlayAniTime); // overlay animation in time
  }

  vm.helloLinkHide = true;
  vm.stateChangeLoaded = function(){

    vm.activeMenuTab = $state.current.name == 'project.detail' ? null : $state.current.name ;
    $state.current.name == 'hello' ? vm.helloLinkHide = true :  vm.helloLinkHide = false;

    $timeout(function(){
      vm.stateChangeActive = false;
    }, 500 ); // hide black overlay after loaded and animated
    $timeout(function(){
        vm.changeClass = null
        helloSecLoad = true;
    },1000);//animation lenght -> 1000
  }


  $rootScope.$on('$viewContentLoaded', function(event) {
    vm.stateChangeLoaded();
  });

  vm.$onInit = function(){
    vm.loaderClass = 'ani-line';
    $timeout(function () {
      vm.loaderClass = 'ani-line move-in hide';
    }, 700); //1600 -> loader static time
  }

};

//**********************************
// Hello controller
//***********************************
function helloCtrl($scope, $state, $rootScope ) {
  var vm = this;
  vm.helloMoveInDelay = 1000;
  vm.helloWriteDelay = vm.helloMoveInDelay + 0;
};


//************************************************
// Directives -> scroll Trigger MAIN
//************************************************

// var helloSecLoad ;
var helloSecLoad = false;
jkApp.directive('helloMoveIn', function($timeout){
  return{
    link: function(scope, element, attrs){
      if(helloSecLoad){
        element[0].classList.add('move-in-fast');
        return;
      }
      $timeout(function(){
        element[0].classList.add('move-in');
        helloSecLoad = true;
      }, scope.vm.helloMoveInDelay);
    }
  }
});

jkApp.directive('swipeStateChange', function($timeout, $state){
  return{
    link: function(scope, element, attrs){

      var hammer    = new Hammer.Manager(element[0]);
      var swipe     = new Hammer.Swipe();

      hammer.add(swipe);
      hammer.on('swipeleft', function(){
        scope.$parent.vm.menuStateChange('contact', null);
      });
      hammer.on('swiperight', function(){
        scope.$parent.vm.menuStateChange('about', null);
      });
      hammer.on('swipeup', function(){
        scope.$parent.vm.menuStateChange('digital', null);

      });
      hammer.on('swipedown', function(){
        scope.$parent.vm.menuStateChange('analog', null);
      });

      scope.$on('$destroy', function() {
        hammer.destroy();
      });

    }
  }
});


jkApp.directive('imgLoader', function($timeout){
  return{
    link: function(scope, element, attrs){
      element[0].classList.add('loading');
      var loader = document.createElement("span");
      loader.classList.add('loader');
      element.unbind('load');
      element.bind("load",function(e){
        element[0].classList.remove('loading');
      });
    }
  }
});

jkApp.directive('disableRighClick', function($timeout){
  return{
    link: function(scope, element, attrs){
      element.unbind('contextmenu');
      element.bind("contextmenu",function(e){
        e.preventDefault();
      });
    }
  }
});


jkApp.directive('delayLoad', function($timeout){
  return{
    scope: {
      srcLoad: '='
    },
    link: function(scope, element, attrs){
        $timeout(function(){
          element[0].src = scope.srcLoad;
        }, attrs.delayLoad );
    }
  }
});


jkApp.directive('randomMove', function($timeout, $window, $interval){
  return{
    // restrict: 'A',
    scope: {
      timeinterval: '=',
      rndtimemax: '=',
      movemax: '='
    },
    link: function(scope, element, attrs){

      // DEFAULT SETTINGS moveMax = 50; rndTimeMax = 8000; timeInterval = 1500;

      var moveMax = scope.movemax;
      if($window.innerWidth < 600) moveMax = moveMax / 3 ;
      var rndTimeMax = scope.rndtimeMax;
      var timeInterval = scope.timeinterval;

      var randomMove = Math.floor(Math.random() * moveMax*2) - moveMax ;
      var randomTime = Math.floor(Math.random() * rndTimeMax);
      var elOrigStyle = element[0].style.left;

      var randomInterval =  $interval(function(){
        randomBoolean = Math.round(Math.random()) ;
        if(randomBoolean == 1){
          element[0].style.left = elOrigStyle + (randomMove + 'px');
          $timeout(function() {
            element[0].style.left = elOrigStyle ;
          },50)
        }
        // random move according to random timeout  $randomTime
        $timeout(function(){
          randomMove = Math.floor(Math.random() * moveMax*2) - moveMax ;
          element[0].style.left = elOrigStyle + (randomMove + 'px');
          $timeout(function(){
            element[0].style.left = elOrigStyle;
          },50)
        },randomTime);
      },timeInterval);

      scope.$on('$destroy', function() {
         $interval.cancel(randomInterval);
      });

    }
  }
});

jkApp.firstHelloStateWas = false;

jkApp.directive('stringWrite', function($timeout, $interval, $rootScope){
  return{
    link: function(scope, element, attrs){

      if(jkApp.firstHelloStateWas == true ) return;
      jkApp.firstHelloStateWas = true;

      var stringToAnimate = element[0].innerText;
      var arrayFromString = stringToAnimate.split('');
      var arrayAnimated = [];
      var aniSpeed = 35 ;
      element[0].innerHTML = "<span style='opacity:0 '>.</span>";
      var i = 0;
      var animateStringInt;

      var stopInt = function() {  $interval.cancel(animateStringInt) }
      var addString = function() {
        arrayAnimated.push(arrayFromString[i]);
        element[0].innerHTML = arrayAnimated.join('');
        i++
        if( arrayFromString.length == arrayAnimated.length){
          stopInt();
          element[0].classList.add('in-out-ani');
          $timeout(function(){
            element[0].classList.remove('active');
            element[0].classList.remove('in-out-ani');
          },2500)
        }
      }

      $rootScope.$on('$viewContentLoaded', function(event) {
        $timeout(function(){
          element[0].classList.add('active');
          animateStringInt = $interval(function(){ addString(); }, aniSpeed);
        },scope.vm.helloWriteDelay);
      });

    }
  }
})
