
var fs = require('fs');
var gulp = require('gulp');
var sass = require('gulp-sass'); // Sass engine
var autoprefixer = require('gulp-autoprefixer'); // prefixies
var rename = require('gulp-rename'); //
var uglify = require('gulp-uglify'); // uglify JS
var concat = require('gulp-concat'); // laczenie plikow
var replace = require('gulp-replace'); // regexy i usuwanie stringow
var bytediff = require('gulp-bytediff');
var runSequence = require('run-sequence');
var ngAnnotate = require('gulp-ng-annotate');
var plumber = require('gulp-plumber');
var templateCache = require('gulp-angular-templatecache');
var htmlmin = require('gulp-htmlmin');


var handlebars = require('gulp-compile-handlebars');
var rev = require('gulp-rev');
const revDelete = require('gulp-rev-delete-original');
var revDel = require('rev-del');

var ulgifyConfig = {
  sourceMap: false,
  preserveComments: false,
  mangle: false,
  beautify: false,
  reserveDOMProperties: true,
  mangleProperties: false,
  screwIE8: true
};

// https://github.com/dlmanning/gulp-sass
var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'compressed'
};

// http://caniuse.com/
var autoprefixerOptions = {
  browsers: ['last 2 versions', '> 2%', 'Firefox ESR']
};


// create a handlebars helper to look up
// fingerprinted asset by non-fingerprinted name
var handlebarOpts = {
	helpers: {
		assetPath: function (path, context) {
			return ['assets/src', context.data.root[path]].join('/');
		}
	}
};

var appHTMLCompileOptions = {
  standalone:true,
  module: 'jkApp.templates'
}
gulp.task('app-html-compile', function () {
 return gulp.src('../app/**/*.html')
 .pipe(htmlmin({collapseWhitespace: true}))
 .pipe(templateCache('templates.js', appHTMLCompileOptions))
 .pipe(gulp.dest('../app/templates'));
});

gulp.task('delete-old-versions' , ()=>{
  fs.readdir('src', (err, files) => {
    var manifest = JSON.parse(fs.readFileSync('rev-manifest.json', 'utf8'));
    files.forEach(file => {
      var filePath = 'src/'+file;
      fs.unlink(filePath , (err) => {
        if (err) throw err;
        console.log('path/file.txt was deleted');
      });
    });
  })
})

gulp.task('versioning-html', ['delete-old-versions', 'app-html-compile' , 'sass', 'ng-app-concat'] , function () {
	var manifest = JSON.parse(fs.readFileSync('rev-manifest.json', 'utf8'));

	return gulp.src('index.hbs')
		.pipe(handlebars(manifest, handlebarOpts))
		.pipe(rename('index.html'))
		.pipe(gulp.dest('../'));
});

gulp.task('sass', function() {
    return gulp.src('./scss/*.scss')
      .pipe(sass(sassOptions).on('error', sass.logError))
      .pipe(autoprefixer(autoprefixerOptions))
      .pipe(gulp.dest('src'))
      .pipe(rev())
      .pipe(revDelete())
      .pipe(gulp.dest('src'))
      .pipe(rev.manifest({
        base: 'src../',
        merge: true
      }))
      .pipe(gulp.dest('./src'))
});


gulp.task('concat_scripts', function() {
  return gulp.src([ './js/common/*.js', './js/common/*.min.js'
    ])
    .pipe(concat('main.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./js'));
});

gulp.task('ng-app-concat', function() {
    return gulp.src(['../app/*.js' , '../app/**/*.js'])
	    .pipe(plumber())
			.pipe(concat('appJK.js', {newLine: ';'}))
			.pipe(ngAnnotate({add: true}))
	    .pipe(plumber.stop())
      .pipe(gulp.dest('src'))
      .pipe(rev())
      .pipe(revDelete())
      .pipe(gulp.dest('src'))
      .pipe(rev.manifest({
        base: 'src../',
        merge: true
      }))
      .pipe(gulp.dest('src/'));

});

gulp.task('app-prod' , function() {
	return gulp.src('src/*.js')
		.pipe(plumber())
			.pipe(bytediff.start())
				.pipe(uglify({mangle: true}))
			.pipe(bytediff.stop())
		.pipe(plumber.stop())
		.pipe(gulp.dest('src/'));
});

gulp.task('watch', () => {
    gulp.watch('../app/**/*.html', ['versioning-html'])
    .on('change', (event) => {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasksHTML...');
    });
    gulp.watch('./scss/*.scss', ['versioning-html'])
    .on('change', (event) => {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasksCSS...');
    });
    gulp.watch(['../app/*.js', '../app/**/*.js'], ['versioning-html'])
    .on('change', (event) => {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasksAPP...');
    });
    runSequence('versioning-html' ,'concat_scripts' ,  function(){
      console.log('done');
    });
});

gulp.task('prod', function() {
  runSequence( 'concat_scripts', 'versioning-html' , 'app-prod', function(){
    console.log('production comliled');
  });
});
