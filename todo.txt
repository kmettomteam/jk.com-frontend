

TODO
-------------------------


-> page speed TEST more

-> chrome mobile viewport height and tab check behavior


1.

2.
TEST:
-> animation state change error after project detail picture change -> Test/Duplicate

3.
-> JSON to BACKEND for always fresh data. OR JSON versioning.
4.

-------------------------------------------------------------

RWD MOBILE
-> project detail content center - (mobile browser tab height) not good solution, or try more???


PICTURE size

One folder with project name. Inside the folder:
a. 1 project thumb picture "thumb.xxx" (can be any format) -> w: 300px / h: 225px (must have these proportions)
b. large pictures named 01.jpg, 02.jpg, 03.jpg -> max width 600px, max height 675px
c. small detail thumb pictures 01s.jpg, 02s.jpg, 03s.jpg  -> max width 130px

PS: large picture (b) and small picture (c) can be different format then "jpg", but they all have to be the same format.
